package com.devcamp.d10.rainbowrequestinputrestapi.models;

import java.util.Arrays;

public class Rainbow {
    private String[] rainbows;

    public Rainbow() {
    }

    public Rainbow(String[] rainbows) {
        this.rainbows = rainbows;
    }

    public String[] getRainbows() {
        return rainbows;
    }

    public void setRainbows(String[] rainbows) {
        this.rainbows = rainbows;
    }

    @Override
    public String toString() {
        return "Rainbow [rainbows=" + Arrays.toString(rainbows) + "]";
    }

}
