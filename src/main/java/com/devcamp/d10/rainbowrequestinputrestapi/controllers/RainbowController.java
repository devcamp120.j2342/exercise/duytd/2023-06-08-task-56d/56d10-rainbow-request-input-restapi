package com.devcamp.d10.rainbowrequestinputrestapi.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.d10.rainbowrequestinputrestapi.services.RainbowService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RainbowController {
    @Autowired
    private RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> getFilteredRainbows(@RequestParam(defaultValue = "red") String filter) {

        ArrayList<String> filtered = rainbowService.getFilteredRainbows(filter);

        return filtered;
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getColorByIndex(@PathVariable() int index) {
        String indexColor = rainbowService.getColorByIndex(index);

        return indexColor;
    }
}
