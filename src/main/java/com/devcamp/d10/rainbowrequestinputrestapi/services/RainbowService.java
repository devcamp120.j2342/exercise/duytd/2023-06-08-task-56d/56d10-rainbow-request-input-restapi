package com.devcamp.d10.rainbowrequestinputrestapi.services;

import java.util.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class RainbowService {

    private String[] rainbows = { "red", "orange", "yellow", "green", "blue", "indigo", "violet" };

    public ArrayList<String> getFilteredRainbows(@RequestParam(defaultValue = "red") String filter) {

        ArrayList<String> filteredRainbows = new ArrayList<>();

        for (String rainbowElement : rainbows) {
            if (rainbowElement.contains(filter)) {
                filteredRainbows.add(rainbowElement);
            }
        }

        return filteredRainbows;
    }

    public String getColorByIndex(@PathVariable("id") int index) {
        if (index >= 0 && index < rainbows.length) {
            return rainbows[index];
        } else {
            return "";
        }
    }
}
